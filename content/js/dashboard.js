/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.919157485631474, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5528741865509761, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999358666025333, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.9789365149506555, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7117327117327117, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.1871569703622393, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.891494532199271, 500, 1500, "me"], "isController": false}, {"data": [0.8795953614606464, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.5715465052283984, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.9767237242041273, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.49451013513513514, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 60723, 0, 0.0, 241.0370864417102, 10, 2711, 119.0, 663.0, 818.0, 1456.0, 202.08666134185304, 973.9462888024992, 252.0200361712926], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1844, 0, 0.0, 811.747288503254, 240, 1931, 823.0, 1095.5, 1187.75, 1362.7499999999998, 6.13999453926733, 3.8794692059628533, 7.597044024659869], "isController": false}, {"data": ["getLatestMobileVersion", 31185, 0, 0.0, 47.66766073432721, 10, 1074, 30.0, 100.0, 148.0, 221.0, 103.96836774496828, 69.54915225127273, 76.65636489008891], "isController": false}, {"data": ["findAllConfigByCategory", 6789, 0, 0.0, 220.1019295919878, 30, 1139, 192.0, 387.0, 484.0, 685.6000000000022, 22.63331955353452, 25.595101604485322, 29.396791998243074], "isController": false}, {"data": ["getNotifications", 2574, 0, 0.0, 580.9335664335673, 55, 1670, 591.0, 982.0, 1098.25, 1234.25, 8.580486227552894, 71.33367114761504, 9.544115051936275], "isController": false}, {"data": ["getHomefeed", 911, 0, 0.0, 1643.4489571899014, 674, 2711, 1609.0, 2143.6000000000004, 2258.2, 2573.04, 3.031835941400036, 36.94161820395337, 15.171022816146273], "isController": false}, {"data": ["me", 4115, 0, 0.0, 363.26877278250305, 60, 1191, 322.0, 643.4000000000001, 744.7999999999993, 900.6800000000003, 13.721011653690335, 18.05434676710125, 43.481135562719864], "isController": false}, {"data": ["findAllChildrenByParent", 4053, 0, 0.0, 368.8613372810269, 53, 1337, 314.0, 688.0, 805.2999999999997, 1000.46, 13.511080886470918, 15.173577167423394, 21.5860628225258], "isController": false}, {"data": ["getAllClassInfo", 1817, 0, 0.0, 822.9928453494779, 98, 1933, 839.0, 1198.2, 1264.0, 1489.5599999999995, 6.0573531666922245, 17.38194716755399, 15.551544409408066], "isController": false}, {"data": ["findAllSchoolConfig", 6251, 0, 0.0, 239.07486802111657, 36, 1193, 214.0, 408.0, 494.0, 715.0, 20.842502567385534, 454.54567122544313, 15.28585881651029], "isController": false}, {"data": ["getChildCheckInCheckOut", 1184, 0, 0.0, 1002.2457770270273, 352, 2099, 987.0, 1255.5, 1331.75, 1615.3000000000002, 3.9434987226927705, 262.9320071209462, 18.146255841140952], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 60723, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
